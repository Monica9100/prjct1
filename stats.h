/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file <stats.h> 
 * @brief <assignment1 >
 *
 * <Add Extended Description Here>
 *
 * @author < Monica Aelavarthi>
 * @date <08/06/2020 >
 *
 */
#ifndef __STATS_H__
#define __STATS_H__

/** 
* @brief prints the elements of the array
*
* the function takes the input of unsigned char array and size of the array. every element of the array is prented sequentially according to its address.

* @param test the array of elements
* @param SIZE the size of the array 
* @param i address of the elements
* @return prints the array
*/
char print_array(unsigned char test[], int SIZE);



/**
* @brief finds the median of the elements of the input array
*
* the function takes the input of unsigned char array and size of the array.The size of the array is checked to be odd or even by dividing it with 2. In case of even size of the array, the median is the average of the middle two ((SIZE-2)/2th and (SIZE/2)th) elements. In case the size is odd, then the (SIZE-1)/2th element becomes the median.
*
* @param test the array of elements
* @param SIZE the size of the array 
* @return the median of the array
*/
float find_median(unsigned char test[], int SIZE);


/**
*@brief finds the mean of the elements of the input array
* 
* the function takes the input unsigned char gtest array and the sze of the array. the sum of all the elements is computed and is divided by the size to obtain the mean.
*
* @param test the array of elements
* @param SIZE the size of the array 
* @param sum of the elements of the array
* @param i address of the elements of the array
* @return the median of the array
*/
float find_mean(unsigned char test[], int SIZE);


/**
*@brief finds the maximum of the elements of the input array
* 
* The function takes the input unsigned char test array and the size of the array. Variable max which is initialised with the first element of the array is compared with the other elemnts sequentially.When the other element is greater than max, the max variale takes the value of that element and continues the comparision till the last element.
*
* @param test the array of elements
* @param SIZE the size of the array 
* @param max assumed maximum value of the array
* @param i address of the elements of the array
* @return the maximum of the array
*/
float find_maximum(unsigned char test[], int SIZE);


/**
*@brief finds the minimum of the elements of the input array
* 
 The function takes the input unsigned char test array and the size of the 
 array. Variable min which is initialised with the first element of the array 
 is compared with the other elemnts sequentially.When the other element is greater than min, the min variale takes the value of that element and continues the comparision till the last element.
*
* @param test the array of elements
* @param SIZE the size of the array 
* @param max assumed maximum value of the array
* @param i address of the elements of the array
* @return the minimum of the array
*/
float find_minimum(unsigned char test[], int SIZE);


/**
*@brief sort gives the descending order of the elements of the input array
* 
* The function takes the input unsigned char test array and the size of the array.the bubblesort algorithm is used in the sorting of the elements of the array. 
*
* @param test the array of elements
* @param SIZE the size of the array 
* @param i the address of the element which is being compared in that cycle
* @param j address of the element to which the ith element is compared to.
* @return the descending order of the array
*/
unsigned char sort(unsigned char test[] , int SIZE);


/** 
* @brief it prints the statics like mean, median , maximum elemen and minimum element of the input array.
*
* To display the mean,the find_mean() function was used in the main and the mean * value was stored in the variable l.
* To display the median, the finf_median was used in the main() and the median * * value is stored in k. the variable is passed into the function and printed.

 To display the maximum value of the array, the find_maximum function is used * * and the max value is stored in variable m whis is passed into the function.

 To display the minimum value of the array, the find_minimum function is used * * and the max value is stored in variable n whis is passed into the function.
*
* @param k this is te value of the median 
* @param l this is the value of the mean
* @param m maxmum value of the array
* @param n minimum value of the array
*/
float print_statistics(unsigned char test[], int SIZE); 



#endif /* __STATS_H__ */


 
