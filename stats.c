/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.c 
 * @brief implements the statistical functions on the given data
 *
 * <Add Extended Description Here>
 *
 * @author Monica Aelavarthi
 * @date 08/06/2020
 *
 */


#include <stdio.h>

/* Size of the Data Set */
int SIZE=40;

void main() {

 unsigned char test[] = { 34, 201, 190, 154,   8, 194,   2,   6,
                              114, 88,   45,  76, 123,  87,  25,  23,
                              200, 122, 150, 90,   92,  87, 177, 244,
                              201,   6,  12,  60,   8,   2,   5,  67,
                                7,  87, 250, 230,  99,   3, 100,  90};

  /* Other Variable Declarations Go Here */

int print_statistics(int k,int l, int m, int n);
char print_array(unsigned char test[], int SIZE);
float find_median(unsigned char test[], int SIZE);
float find_mean(unsigned char test[], int SIZE);
float find_maximum(unsigned char test[], int SIZE);
float find_minimum(unsigned char test[], int SIZE);
unsigned char sort(unsigned char test[] , int SIZE);

  /* Statistics and Printing Functions Go Here */
find_median(test, SIZE);
int k=find_median(test,SIZE);

find_mean(test, SIZE);
int l=find_mean(test, SIZE);

find_maximum(test, SIZE);
int m=find_maximum(test,SIZE);

find_minimum(test, SIZE);
int n=find_minimum(test, SIZE);

print_statistics(k,l,m,n);

print_array(test,SIZE);

sort(test, SIZE);

}

/* Add other Implementation File Code Here */

int print_statistics(int k,int l, int m ,int n)
{
 
printf("\n maximum value of the array=%d", m);
 
printf("\n minimum value of the array=%d", n);

printf("\n mean value of the array=%d", l);

printf("\n the median of the array=%d", k);


}

char print_array( unsigned char test[], int SIZE)
{
int i;
printf("\nprinting the array");
printf("\n");
 for (i=0; i<SIZE; i++)
 {
  printf(" \t %d", test[i]);
 }
}

float find_median( unsigned char test[],int SIZE)
{
int median;
if(SIZE%2==0)
{
median=(test[(SIZE-2)/2]+test[SIZE/2])/2;
}
else 
median=test[(SIZE-1)/2];
return median;
}

float find_mean(unsigned char test[], int SIZE)
{
 int i,sum=0;
 for(i=0;i<SIZE;i++)
{
 sum=sum+test[i];

}
return sum/SIZE;
}

float find_maximum(unsigned char test[], int SIZE)
{
int i, max=0;
for(i=0;i<SIZE;i++)
  {   
   if (max<test[i])
    {
      max=test[i];
    }
   }
return max;
}

float find_minimum(unsigned char test[], int SIZE)
{
int i, min=0;
for(i=0;i<SIZE;i++)
  {   
   if (min>test[i])
    {
      min=test[i];
    }
   }
return min;
}

unsigned char sort(unsigned char test[] , int SIZE)
{
int i,j,x;
for(i=0; i< SIZE; i++)
{
for(j=0; j< SIZE; j++)
{
if(test[i]>test[j])
{
x=test[i];
test[i]=test[j];
test[j]=x;
}
}
}
printf("\nsorted array");
printf("\n");
for (i=0;i<SIZE;i++)
{
printf("\t %d", test[i]);
}
}
